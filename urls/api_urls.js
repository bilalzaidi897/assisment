const urls={
    "/api/admin/modules/admins/v1/login":true,

    "/api/admin/modules/faculties/v1/addFaculty":true,
    "/api/admin/modules/faculties/v1/updateFaculty":true,
    "/api/admin/modules/faculties/v1/deleteFaculty":true,
    "/api/admin/modules/faculties/v1/getFaculties":true,

    "/api/admin/modules/students/v1/addStudent":true,
    "/api/admin/modules/students/v1/updateStudentDetails":true,
    "/api/admin/modules/students/v1/deleteStudent":true,
    "/api/admin/modules/students/v1/getStudents":true,

    "/api/admin/modules/subjects/v1/addSubject":true,
    "/api/admin/modules/subjects/v1/updateSubjectDetails":true,
    "/api/admin/modules/subjects/v1/deleteSubject":true,
    "/api/admin/modules/subjects/v1/getSubjects":true
};
module.exports={urls};