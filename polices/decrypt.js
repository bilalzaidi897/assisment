const { decrypt, unauthorizedResponse } = require("../lib/universal-function");

const decryptAuth = async (req, res, next) => {
  try {
    if (req.headers && req.headers.authorization) {
      const accessToken = req.headers.authorization;

      const decryptData = await decrypt(accessToken);
      req.headers["authorization"] = decryptData;
      next();
    } else {
      return unauthorizedResponse(req, res);
    }
  } catch (error) {
    throw error;
  }
};
module.exports = { decryptAuth };
