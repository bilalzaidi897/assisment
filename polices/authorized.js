const {
  unauthorizedResponse,
  getCallerIP,
  jwtVerify,
} = require("../lib/universal-function");
const {
  setData,
  setTime,
  getData,
} = require("../services/redis");

const { LANGUAGE_TYPE } = require("../constant/constant");
const validateAuth=async(accessToken)=>{
  try {
    let authData=await jwtVerify(accessToken);
    let tokenData=await getData(accessToken);
    if(authData && tokenData ==accessToken){
      return {data:authData};
    }
    else return {data:null};
  } catch (error) {
    throw error;
  }
}
const authorization = async (req, res, next) => {
  try {
    if (req.headers && req.headers.authorization) {
      const accessToken = req.headers.authorization;
      const lang = req.headers["content-language"] || LANGUAGE_TYPE.ENGLISH;
      const {error,data} = await validateAuth(accessToken);
      if (error || !data) return unauthorizedResponse(req, res);
      req.authData = data;
      req.authData.lang = lang;
      next();
    } else {
      return unauthorizedResponse(req, res);
    }
  } catch (error) {
    console.log(error)
    return unauthorizedResponse(req, res);
  }
};
module.exports = { authorization };
