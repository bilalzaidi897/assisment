const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const path = require('path');
const config = require("config");
const cors = require('cors');
const {redisConnection,mysqlConnection} = require("./connection/connect");
const route = require("./modules/route");
const {sendErrorResponse} = require("./lib/universal-function");
const { codes } = require("./codes/status_codes");
const { logger } = require("./services/logger");
const { urls } = require("./urls/api_urls");
const {SERVICE_NAME}=require("./constant/constant")
const server = require("http").createServer(app);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use('/static', express.static(path.join(__dirname, './uploads/')));

app.use((req, res, next) => {
    if(urls[req.url]){
        let payload=req.body || req.query || req.params;
        let url=req.url;
        logger.log('info', {url,payload});
    }
    next();
});
app.use("/api/"+SERVICE_NAME, route);

app.use((error, req, res, next) => {
    if(urls[req.url]){
        let payload=req.body || req.query || req.params;
        let url=req.url;
        logger.log('error', {url,payload,error});
    }
    return sendErrorResponse(req, res, codes.BAD_REQUEST, error.message || error);
});

server.listen(config.get("port"), async () => {
    logger.log('info', `Environment :${process.env.NODE_ENV}`);
    logger.log('info', `Running on :${config.get("port")}`);
    await redisConnection();
    await mysqlConnection();
});

