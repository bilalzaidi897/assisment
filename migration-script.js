
const mysql     = require('mysql');
const migration = require('mysql-migrations');
const config = require("config");

const mysqlMasterConfig =  config.get('mysqlMasterDatabaseSettings');
var connection = mysql.createPool(mysqlMasterConfig);

migration.init(connection, __dirname + '/migrations', function() {
  console.log("finished running migrations");
});