const {
  validateAddFaculty,
  validateUpdateFaculty,
  validateDeleteFaculty,
  validateGetFaculties,
} = require("../validations/faculty");
const _ = require("underscore");
const {
  getFacultyData,
  insertFacultyData,
  insertFacultySubjectMapping,
  updateFacultyRecord,
  updateFacultySubjectMapping,
  getFacultySubjectsMapping,
} = require("../services/faculty");
const { FILE_PATH } = require("../../../../constant/constant");

const { sendResponse } = require("../../../../lib/universal-function");
const { messages } = require("../../../../messages/messages");
const { codes } = require("../../../../codes/status_codes");
/**
 * @description used for en-rolled/add faculty
 * @input params {first_name, last_name, dob, contact_number, email, subjects}
 * @returns faculty id
 */
const addFaculty=async(req, res, next)=> {
  try {
    await validateAddFaculty(req);

    let { first_name, last_name, dob, contact_number, email, subjects } =
      req.body;
    let checkFacultyExist = await getFacultyData({
      email,
      contact_number,
      is_deleted: 0,
    });

    if (!_.isEmpty(checkFacultyExist)) {
      if (checkFacultyExist[0].email == email) {
        throw messages.EMAIL_ALRADY_EXIST;
      }
      throw messages.PHONE_NO_ALREADY_EXIST;
    }
    let facultyData = await insertFacultyData({
      first_name,
      last_name,
      dob,
      contact_number,
      email,
    });
    if (subjects.length) {
      await insertFacultySubjectMapping({
        faculty_id: facultyData.insertId,
        subjects,
      });
    }
    return sendResponse(req, res, codes.SUCCESS, messages.FACULTY_ADDED, {
      faculty_id: facultyData.insertId,
    });
  } catch (error) {
    next(error);
  }
}
/**
 * @description used for update faculty details
 * @input params {faculty_id, contact_number, subject_to_remove, subject_to_add }
 * @returns empty object 
 */
const updateFaculty=async(req, res, next)=> {
  try {
    await validateUpdateFaculty(req);
    let { faculty_id, contact_number, subject_to_remove, subject_to_add } =
      req.body;

    let checkFacultyExist = await getFacultyData({ faculty_id, is_deleted: 0 });
    if (_.isEmpty(checkFacultyExist)) {
      throw messages.INVALID_FACULTY_ID;
    }
    if (
      contact_number &&
      checkFacultyExist[0].contact_number == contact_number
    ) {
      throw messages.PHONE_NO_ALREADY_EXIST;
    }
    await updateFacultyRecord(req.body);
    if (!_.isEmpty(subject_to_remove)) {
      await updateFacultySubjectMapping({
        is_deleted: 1,
        faculty_id,
        subject_ids: subject_to_remove,
      });
    }
    if (!_.isEmpty(subject_to_add)) {
      await insertFacultySubjectMapping({
        faculty_id,
        subjects: subject_to_add,
      });
    }
    return sendResponse(
      req,
      res,
      codes.SUCCESS,
      messages.FACULTY_DATA_UPDATED,
      {}
    );
  } catch (error) {
    next(error);
  }
}
/**
 * @description used for delete particular faculty
 * @input params {faculty_id}
 * @returns empty object
 */
const deleteFaculty=async(req, res, next)=> {
  try {
    await validateDeleteFaculty(req);
    let { faculty_id } = req.body;

    let checkFacultyExist = await facultyService.getFacultyData({ faculty_id });
    if (_.isEmpty(checkFacultyExist)) {
      throw messages.INVALID_FACULTY_ID;
    }
    if (!_.isEmpty(checkFacultyExist) && checkFacultyExist[0].is_deleted) {
      throw messages.FACULTY_ALREADY_DELETED;
    }
    let compositeQuery = Promise.all([
      updateFacultyRecord({ is_deleted: 1, faculty_id }),
      updateFacultySubjectMapping({ is_deleted: 1, faculty_id }),
    ]);
    await compositeQuery;
    return sendResponse(req, res, codes.SUCCESS, messages.FACULTY_DELETED, {});
  } catch (error) {
    next(error);
  }
}
/**
 * @description used for details of faculty
 * @input params {faculty_id}
 * @returns empty object if not find other return details of faculty
 */
const getFaculties=async(req, res, next)=> {
  try {
    await validateGetFaculties(req);
    let { faculty_id } = req.query;

    let facultyDetails = await getFacultyData({ faculty_id, is_deleted: 0 });

    let faculty_ids = facultyDetails.map((x) => x["id"]);
    if (_.isEmpty(faculty_ids)) {
      return sendResponse(req, res, codes.SUCCESS, messages.SUCCESS, {});
    }
    let getSubjectMapping = await getFacultySubjectsMapping({
      faculty_ids: faculty_ids,
      is_deleted: 0,
    });
    let facultySubjectMap = {};
    for (let i = 0; i < getSubjectMapping.length; i++) {
      if (!facultySubjectMap[getSubjectMapping[i].faculty_id]) {
        facultySubjectMap[getSubjectMapping[i].faculty_id] = [
          getSubjectMapping[i],
        ];
      } else {
        facultySubjectMap[getSubjectMapping[i].faculty_id].push(
          getSubjectMapping[i]
        );
      }
    }
    for (let i = 0; i < facultyDetails.length; i++) {
      if (facultySubjectMap[facultyDetails[i].id]) {
        facultyDetails[i].subjects = facultySubjectMap[facultyDetails[i].id];
      } else {
        facultyDetails[i].subjects = [];
      }
    }
    return sendResponse(req, res, codes.SUCCESS, messages.SUCCESS, 
      facultyDetails,
    );
  } catch (error) {
    next(error);
  }
}
/**
 * @description used for faculty file upload for profile
 * @input params {image}
 * @returns file url
 */
const uploadFile=async(req, res, next)=> {
  try{
    let data = {};
    if (req.file && req.file.filename) {
      data.imageUrl = `${FILE_PATH.FACULTY}/${req.file.filename}`;
    }
    return sendResponse(req, res, codes.SUCCESS, messages.FILE_UPLOADED_SUCCESSFULLY, data);
  }catch (error) {
    next(error);
  }
}
module.exports = {
  addFaculty,
  updateFaculty,
  deleteFaculty,
  getFaculties,
  uploadFile,
};
