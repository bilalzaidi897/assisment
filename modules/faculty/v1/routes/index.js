const {
  addFaculty,
  updateFaculty,
  deleteFaculty,
  getFaculties,
  uploadFile
} = require("../controllers/faculty");
const {faculty} = require('../../../../services/fileUploadService');
const { authorization } = require("./../../../../polices/authorized");

const express = require("express");
const router = express.Router();

/*
Faculties routes
*/

router.post("/addFaculty", authorization, addFaculty);
router.put("/updateFaculty", authorization, updateFaculty);
router.put("/deleteFaculty", authorization, deleteFaculty);
router.get("/getFaculties", authorization, getFaculties);

router.post('/uploadFile',authorization,faculty.single('image'),uploadFile);

module.exports = router;
