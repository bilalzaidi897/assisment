const {
    validateLogin,
} = require("../validations/admin");

const {sendResponse,
    comparePasswordUsingBcrypt,
    hashPasswordUsingBcrypt,
    jwtSign} = require("../../../../lib/universal-function");
const {messages} = require("../../../../messages/messages");
const {codes} = require("../../../../codes/status_codes");
const {getAdminDetails} = require("../services/admin");

const {
    setData,
    setTime,
    getData,
} = require("../../../../services/redis");

/**
 * @description used for details of admin data and accessToken
 * @input params {email,password}
 * @returns accessToken with admin data
 */

const login=async(req, res, next)=>{
    try {
        await validateLogin(req);
        let {email,password}=req.body;
        let adminDetails=await getAdminDetails({email});
        if(Array.isArray(adminDetails) && adminDetails.length){
            let isSamePassword=await comparePasswordUsingBcrypt(password,adminDetails[0].password);
            if(!isSamePassword){
                throw messages.INVALID_PASSWORD;
            }else{
                let payload={
                    ...adminDetails[0],
                }
                delete payload.password; 
                payload.accessToken=await jwtSign(payload);
                await setData(payload.accessToken,payload.accessToken); // used for validation of accessToken by redis db
                await setTime(payload.accessToken,360*24*60*60*1000);  // expired after one year
                return sendResponse(req, res, codes.CREATED, messages.LOGIN, payload);
            }
        }else{
            throw messages.INVALID_EMAIL_OR_PASSWORD;
        }
        
    } catch (error) {
        next(error);
    }
};
/**
 * @description used for details of admin
 * @input params {accessToken}
 * @returns details of admin
 */
const accessToken=async(req, res, next)=>{
    try {
        let {id}=req.authData;
        let adminDetails=await getAdminDetails({admin_id:id});
        if(Array.isArray(adminDetails) && adminDetails.length){
            adminDetails=adminDetails[0];
        }else{
            adminDetails={};
        }
        return sendResponse(req, res, codes.CREATED, messages.SUCCESS, adminDetails);
    } catch (error) {
        next(error);
    }
};

module.exports={
    login,
    accessToken
};
