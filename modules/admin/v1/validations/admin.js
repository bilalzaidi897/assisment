const joi = require("joi");
const {validateSchema} = require("../../../../lib/universal-function");

const validateLogin= async (req) => {
    let schema = joi.object().keys({
        email:joi.string().max(100).required(),
        password:joi.string().max(100).required()
    });
    return await validateSchema(req.body, schema);
};

module.exports = {
    validateLogin
};
