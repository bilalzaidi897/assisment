const { login,accessToken } = require("../controllers/admin");
const { authorization } = require("./../../../../polices/authorized");
const express = require("express");
const router = express.Router();

/*
Admin routes
*/
router.put("/login", login);
router.get("/accessToken",authorization, accessToken);
module.exports = router;
