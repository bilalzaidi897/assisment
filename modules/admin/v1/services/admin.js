const { query } = require("../../../../services/mysql");

const getAdminDetails = async (opts) => {
  let values = [];
  let sql = `SELECT * FROM tb_admins WHERE 1`;

  if (opts.admin_id) {
    sql += " AND id = ?";
    values.push(opts.admin_id);
  }
  if (opts.email) {
    sql += " AND email = ?";
    values.push(opts.email);
  }
  if (opts.password) {
    sql += " AND password = ?";
    values.push(opts.password);
  }
  return await query(sql, values);
};
module.exports = {
  getAdminDetails
};
