const router = require('express').Router();

/**
 * Admin,faculty,student and subject routes
 */
const adminRoutes = require('./admin/v1/routes');
const facultyRoutes = require('./faculty/v1/routes');
const studentRoutes = require('./student/v1/routes');
const subjectRoutes = require('./subject/v1/routes');

router.use('/admins/v1',adminRoutes);
router.use('/faculties/v1',facultyRoutes);
router.use('/students/v1',studentRoutes);
router.use('/subjects/v1',subjectRoutes);

module.exports = router;
