const joi = require("joi");
const { validateSchema } = require("../../../../lib/universal-function");

const validateAddStudent = async (req) => {
  const schema = joi.object().keys({
    first_name: joi.string().required(),
    last_name: joi.string().optional(),
    student_class: joi.string().required(),
    dob: joi.string().required(),
    email: joi.string().email().required(),
    contact_number: joi.string().optional().min(9).max(10),
    subjects: joi.array().optional(),
  });
  return await validateSchema(req.body, schema);
};

const validateUpdateStudent = async (req) => {
  const schema = joi
    .object()
    .keys({
      student_id: joi.number().required(),
      first_name: joi.string().optional(),
      last_name: joi.string().optional(),
      student_class: joi.string().optional(),
      contact_number: joi.string().optional(),
      dob: joi.string().optional(),
      subject_to_remove: joi.array().optional(),
      subject_to_add: joi.array().optional(),
    })
    .or(
      "first_name",
      "last_name",
      "class",
      "contact_number",
      "dob",
      "subject_to_remove",
      "subject_to_add"
    );
  return await validateSchema(req.body, schema);
};

const validateDeleteStudent = async (req) => {
  const schema = joi.object().keys({
    student_id: joi.number().required(),
  });
  return await validateSchema(req.body, schema);
};

const validateGetStudents = async (req) => {
  const schema = joi.object().keys({
    student_id: joi.number().optional(),
  });
  return await validateSchema(req.query, schema);
};

module.exports = {
  validateAddStudent,
  validateUpdateStudent,
  validateDeleteStudent,
  validateGetStudents,
};
