const {
  validateAddStudent,
  validateUpdateStudent,
  validateDeleteStudent,
  validateGetStudents,
} = require("../validations/student");
const _ = require("underscore");
const { sendResponse } = require("../../../../lib/universal-function");
const { messages } = require("../../../../messages/messages");
const { codes } = require("../../../../codes/status_codes");
const { FILE_PATH } = require("../../../../constant/constant");

const {
  getStudentData,
  insertStudentData,
  insertStudentSubjectMapping,
  updateStudentRecord,
  updateStudentSubjectMapping,
  getStudentsSubjectsMapping,
} = require("../services/student");
/**
 * @description used for en-rolled/add student
 * @input params {first_name, last_name, dob, contact_number, email, student_class,subjects}
 * @returns student id
 */
const addStudent = async (req, res, next) => {
  try {
    await validateAddStudent(req);
    let {
      first_name,
      last_name,
      dob,
      contact_number,
      email,
      student_class,
      subjects,
    } = req.body;

    let checkStudentExist = await getStudentData({
      email,
      contact_number,
      is_deleted: 0,
    });
    if (!_.isEmpty(checkStudentExist)) {
      if (checkStudentExist[0].email == email) {
        throw messages.EMAIL_ALRADY_EXIST;
      }
      throw messages.PHONE_NO_ALREADY_EXIST;
    }
    let studentData = await insertStudentData({
      first_name,
      last_name,
      dob,
      contact_number,
      email,
      student_class,
    });
    if (subjects && subjects.length) {
      await insertStudentSubjectMapping({
        student_id: studentData.insertId,
        subjects,
      });
    }
    return sendResponse(req, res, codes.SUCCESS, messages.STUDENT_ADDED, {
      student_id: studentData.insertId,
    });
  } catch (error) {
    next(error);
  }
};
/**
 * @description used for update student details
 * @input params {student_id, contact_number, subject_to_remove, subject_to_add }
 * @returns empty object 
 */
const updateStudentDetails = async (req, res, next) => {
  try {
    await validateUpdateStudent(req);
    let { student_id, contact_number, subject_to_remove, subject_to_add } =
      req.body;
    if (contact_number) {
      let checkNumberExist = await getStudentData({
        contact_number,
        is_deleted: 0,
      });
      if (!_.isEmpty(checkNumberExist)) {
        throw messages.PHONE_NO_ALREADY_EXIST;
      }
    }
    await updateStudentRecord(req.body);
    if (!_.isEmpty(subject_to_remove)) {
      await updateStudentSubjectMapping({
        is_deleted: 1,
        student_id,
        subject_ids: subject_to_remove,
      });
    }
    if (!_.isEmpty(subject_to_add)) {
      await insertStudentSubjectMapping({
        student_id,
        subjects: subject_to_add,
      });
    }
    return sendResponse(
      req,
      res,
      codes.SUCCESS,
      messages.STUDENT_DATA_UPDATED,
      {}
    );
  } catch (error) {
    next(error);
  }
};
/**
 * @description used for delete particular student
 * @input params {student_id}
 * @returns empty object
 */
const deleteStudent = async (req, res, next) => {
  try {
    await validateDeleteStudent(req);
    let { student_id } = req.body;

    let checkStudentExist = await getStudentData({ student_id });
    if (_.isEmpty(checkStudentExist)) {
      throw messages.INVALID_STUDENT_ID;
    }
    if (!_.isEmpty(checkStudentExist) && checkStudentExist[0].is_deleted) {
      throw messages.STUDENT_ALREADY_DELETED;
    }
    let compositeQuery = Promise.all([
      updateStudentRecord({ is_deleted: 1, student_id }),
      updateStudentSubjectMapping({ is_deleted: 1, student_id }),
    ]);

    await compositeQuery;
    return sendResponse(req, res, codes.SUCCESS, messages.STUDENT_DELETED, {});
  } catch (error) {
    next(error);
  }
};
/**
 * @description used for details of student
 * @input params {student_id}
 * @returns empty object if not find other return details of student
 */
const getStudents = async (req, res, next) => {
  try {
    await validateGetStudents(req);
    let { student_id } = req.query;

    let studentDetails = await getStudentData({ student_id, is_deleted: 0 });
    let student_ids = studentDetails.map((x) => x["id"]);
    if (_.isEmpty(student_ids)) {
      return sendResponse(req, res, codes.SUCCESS, messages.SUCCESS, {});
    }
    let getSubjectMapping = await getStudentsSubjectsMapping({
      student_ids: student_ids,
      is_deleted: 0,
    });
    let studentSubjectMap = {};
    for (let i = 0; i < getSubjectMapping.length; i++) {
      if (!studentSubjectMap[getSubjectMapping[i].student_id]) {
        studentSubjectMap[getSubjectMapping[i].student_id] = [
          getSubjectMapping[i],
        ];
      } else {
        studentSubjectMap[getSubjectMapping[i].student_id].push(
          getSubjectMapping[i]
        );
      }
    }
    for (let i = 0; i < studentDetails.length; i++) {
      if (studentSubjectMap[studentDetails[i].id]) {
        studentDetails[i].subjects = studentSubjectMap[studentDetails[i].id];
      } else {
        studentDetails[i].subjects = [];
      }
    }
    return sendResponse(req, res, codes.SUCCESS, messages.SUCCESS, 
      studentDetails,
    );
  } catch (error) {
    next(error);
  }
};
/**
 * @description used for student file upload for profile
 * @input params {image}
 * @returns file url
 */
const uploadFile=async(req, res, next)=> {
  try{
    let data = {};
    if (req.file && req.file.filename) {
        data.imageUrl = `${FILE_PATH.STUDENT}/${req.file.filename}`;
    }
    return sendResponse(req, res, codes.SUCCESS, messages.FILE_UPLOADED_SUCCESSFULLY, data);
  }catch (error) {
    next(error);
  }
}
module.exports = {
  addStudent,
  updateStudentDetails,
  deleteStudent,
  getStudents,
  uploadFile
};
