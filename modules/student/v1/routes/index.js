const {
  addStudent,
  updateStudentDetails,
  deleteStudent,
  getStudents,
  uploadFile
} = require("../controllers/student");
const {authorization} = require('./../../../../polices/authorized');
const {student} = require('../../../../services/fileUploadService');
const express = require("express");
const router = express.Router();

/*
Students routes
*/
router.post("/addStudent",authorization,addStudent);
router.put("/updateStudentDetails",authorization,updateStudentDetails);
router.put("/deleteStudent",authorization,deleteStudent);
router.get("/getStudents",authorization,getStudents);
router.post('/uploadFile',authorization,student.single('image'),uploadFile);

module.exports = router;
