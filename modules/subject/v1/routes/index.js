const {
  addSubject,
  updateSubjectDetails,
  deleteSubject,
  getSubjects,
} = require("../controllers/subject");
const { authorization } = require("./../../../../polices/authorized");
const express = require("express");
const router = express.Router();

/*
Subject route
*/

router.post("/addSubject", authorization,addSubject);
router.put("/updateSubjectDetails", authorization,updateSubjectDetails);
router.put("/deleteSubject", authorization,deleteSubject);
router.get("/getSubjects",authorization, getSubjects);

module.exports = router;
