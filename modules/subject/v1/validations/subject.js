const joi = require("joi");
const { validateSchema } = require("../../../../lib/universal-function");

const validateAddSubject = async (req) => {
  const schema = joi.object().keys({
    name: joi.string().required(),
    subject_code: joi.string().required(),
  });
  return await validateSchema(req.body, schema);
};

const validateUpdateSubject = async (req) => {
  const schema = joi
    .object()
    .keys({
      subject_id: joi.number().required(),
      name: joi.string().optional(),
      subject_code: joi.string().optional(),
    })
    .or("name", "subject_code");
  return await validateSchema(req.body, schema);
};

const validateDeleteSubject = async (req) => {
  const schema = joi.object().keys({
    subject_id: joi.number().required(),
  });
  return await validateSchema(req.body, schema);
};

const validateGetSubject = async (req) => {
  const schema = joi.object().keys({
    subject_id: joi.number().optional(),
  });
  return await validateSchema(req.query, schema);
};

module.exports = {
  validateAddSubject,
  validateUpdateSubject,
  validateDeleteSubject,
  validateGetSubject,
};
