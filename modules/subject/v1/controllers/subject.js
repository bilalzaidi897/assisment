const {
  validateAddSubject,
  validateUpdateSubject,
  validateDeleteSubject,
  validateGetSubject,
} = require("../validations/subject");
const _ = require("underscore");

const {
  getSubject,
  insertSubject,
  updateSubject,
} = require("../services/subject");

const { sendResponse } = require("../../../../lib/universal-function");
const { messages } = require("../../../../messages/messages");
const { codes } = require("../../../../codes/status_codes");

/**
 * @description used for en-rolled/add subject
 * @input params { name, subject_code }
 * @returns subject id
 */
const addSubject = async (req, res, next) => {
  try {
    await validateAddSubject(req);
    let { name, subject_code } = req.body;

    let subject = await insertSubject({ name, subject_code });
    return sendResponse(req, res, codes.SUCCESS, messages.SUBJECT_ADDED, {
      subject_id: subject.insertId,
    });
  } catch (error) {
    next(error);
  }
};
/**
 * @description used for update subject details
 * @input params { name, subject_code  }
 * @returns empty object 
 */
const updateSubjectDetails = async (req, res, next) => {
  try {
    await validateUpdateSubject(req);
    let { subject_id } = req.body;
    let checkSubjectExist = await getSubject({ subject_id, is_deleted: 0 });
    if (_.isEmpty(checkSubjectExist)) {
      throw messages.INVALID_SUBJECT_ID;
    }
    await updateSubject(req.body);
    return sendResponse(
      req,
      res,
      codes.SUCCESS,
      messages.SUBJECT_DATA_UPDATED,
      {}
    );
  } catch (error) {
    next(error);
  }
};
/**
 * @description used for delete particular subject
 * @input params {subject_id}
 * @returns empty object
 */
const deleteSubject = async (req, res, next) => {
  try {
    await validateDeleteSubject(req);
    let { subject_id } = req.body;

    let checkSubjectExist = await getSubject({ subject_id });
    if (_.isEmpty(checkSubjectExist)) {
      throw messages.INVALID_SUBJECT_ID;
    }
    if (!_.isEmpty(checkSubjectExist) && checkSubjectExist[0].is_deleted) {
      throw messages.SUBJECT_ALREADY_DELETED;
    }
    await updateSubject({ is_deleted: 1, subject_id });
    return sendResponse(req, res, codes.SUCCESS, messages.SUBJECT_DELETED, {});
  } catch (error) {
    next(error);
  }
};
/**
 * @description used for details of student
 * @input params {subject_id}
 * @returns empty object if not find other return details of subject
 */
const getSubjects = async (req, res, next) => {
  try {
    await validateGetSubject(req);
    let { subject_id } = req.query;

    let subjectDetails = await getSubject({ subject_id, is_deleted: 0 });
    return sendResponse(req, res, codes.SUCCESS, messages.SUCCESS, 
      subjectDetails,
    );
  } catch (error) {
    next(error);
  }
};

module.exports = {
  addSubject,
  updateSubjectDetails,
  deleteSubject,
  getSubjects,
};
