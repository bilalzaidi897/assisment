const { query } = require("../../../../services/mysql");

const getSubject = async (opts) => {
  let values = [];
  let sql = `SELECT * FROM tb_subjects WHERE 1`;

  if (opts.hasOwnProperty("is_deleted")) {
    sql += ` AND is_deleted = 0`;
  }
  if (opts.subject_id) {
    sql += " AND id = ?";
    values.push(opts.subject_id);
  }
  return await query(sql, values);
};

const insertSubject = async (opts) => {
  let sql = `INSERT INTO tb_subjects(name, subject_code) VALUES (?, ?)`;
  let values = [opts.name, opts.subject_code];
  return await query(sql, values);
};

const updateSubject = async (opts) => {
  let updateObj = {};

  opts.hasOwnProperty("is_deleted")
    ? (updateObj.is_deleted = opts.is_deleted)
    : 0;
  opts.hasOwnProperty("name") ? (updateObj.name = opts.name) : 0;
  opts.hasOwnProperty("subject_code")
    ? (updateObj.subject_code = opts.subject_code)
    : 0;

  let sql = "UPDATE tb_subjects SET ? WHERE id = ?";
  return await query(sql, values);
};

module.exports = {
  getSubject,
  insertSubject,
  updateSubject,
};
