const redis = require("redis");
var mysql   = require('mysql');
let P = require("bluebird");
const config = require("config");
const { logger } = require("../services/logger");
const {delay} = require("../lib/universal-function");
const mysqlMasterConfig =  config.get('mysqlMasterDatabaseSettings');
const mysqlSlaveConfig = config.get('mysqlSlaveDatabaseSettings');

P.promisifyAll(redis.RedisClient.prototype);
P.promisifyAll(redis.Multi.prototype);
global.connection;
global.slaveConnection;

const initializeMysqlConnectionPool=async(dbConfig)=>{
    let numConnectionsInPool = 0;
    logger.log('info', `CALLING INITIALIZE POOL`)
    let pool = mysql.createPool(dbConfig);
    pool.on('connection', async (connection) =>{
        numConnectionsInPool++;
        logger.log('info',`CONNECTION IN POOL : ${numConnectionsInPool}`);
    });
    pool.on('error', async (error) =>{
        logger.log('error',`ERROR CONNECTION IN POOL : ${error}`);
        await delay(3000);
        await initializeMysqlConnectionPool(dbConfig);
    });
    return pool;
}
const mysqlConnection = async function () {
    try {
        global.connection = await initializeMysqlConnectionPool(mysqlMasterConfig);
       // global.slaveConnection = initializeMysqlConnectionPool(mysqlSlaveConfig);
    } catch (error) {
        throw error
    }
};
const redisConnection = async () =>{
    try {
        global.redisConnect = redis.createClient(config.get("redis.port"), config.get("redis.host"));
    } catch (error) {
        throw error
    }
};
module.exports = {redisConnection,mysqlConnection};
