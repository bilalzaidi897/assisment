module.exports = {
    DATA_NOT_FOUND: "Data not found.",
    DATABASE_ERROR: "Database error.",
    FORBIDDEN: "Forbidden.",
    IMPLEMENTATION_ERROR: "Implementation error.",
    INTERNAL_SERVER_ERROR: "Internal server error.",
    SUCCESS: "Successfully.",
    TOKEN_ALREADY_EXPIRED: "Token already expired.",
    UNAUTHORIZED: "Unauthorized.",
    LOGOUT_SUCCESSFULLY:"Logout successfully.",
    ALREADY_LOGOUT_SUCCESSFULLY:"Already logout.",
    PLEASE_TRY_AGAIN:"Please try again.",
    LOGIN:"Login successfully.",
    EMAIL_ALRADY_EXIST:"Email already exist.",
    PHONE_NO_ALREADY_EXIST:"Phone no. already exist.",
    INVALID_PASSWORD:"Wrong password entered.",
    INVALID_EMAIL_OR_PASSWORD:"Invalid email or password.",
    
    FACULTY_ADDED:"Faculty added successfully.",
    INVALID_FACULTY_ID:"Invalid faculty data",
    FACULTY_DATA_UPDATED:"Faculty data updated successfully.",
    FACULTY_ALREADY_DELETED:"Faculty already deleted.",
    FACULTY_DELETED:"Faculty deleted successfully.",

    STUDENT_ADDED:"Student added successfully.",
    INVALID_STUDENT_ID:"Invalid student data",
    STUDENT_DATA_UPDATED:"Student data updated successfully.",
    STUDENT_ALREADY_DELETED:"Student already deleted.",
    STUDENT_DELETED:"Student deleted successfully.",

    SUBJECT_ADDED:"Subject added successfully.",
    INVALID_SUBJECT_ID:"Invalid subject data",
    SUBJECT_DATA_UPDATED:"Subject data updated successfully.",
    SUBJECT_ALREADY_DELETED:"Subject already deleted.",
    SUBJECT_DELETED:"Subject deleted successfully.",

    FILE_UPLOADED_SUCCESSFULLY:"File upload successfully."

};
