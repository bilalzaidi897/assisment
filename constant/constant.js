const LANGUAGE_TYPE= {
    DEFAULT: "en",
    ENGLISH: "en",
    ARABIC: "ar",
};

const DEVICE_TYPE= {
    DEFAULT: 0,
    ANDROID: 1,
    IOS: 2,
    WEB: 3,
};
const LOGGER={
    LOGGER_ON:true,
    CONSOLE_ON:true,
    ADD_ERROR_FILE:false,
    ADD_INFO_FILE:false,
    DAILY_ROTATE_LOG_ON:false
}
const REQUEST_METHOD={
    GET:"get",
    POST:"post"
}
const STATUS={
    DEFAULT:0,
    ACTIVE:1,
    DEACTIVE:2,
    DELETED:3
}
const USER_TYPE={
    DEFAULT:0,
    ADMIN:1,
    USER:2,
    AGENT:3,
    CUSTOMER:4
}
const OTP_VALIDATION={
    USER_FORGOT:3
}
const FILE_TYPE={
    CSV:1,
    EXCEL:2
}
const CONTENT_TYPE={
    CSV_FILE_DOWNLOAD:"application/octet-stream",
    EXCEL_FILE_DOWNLOAD:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
}
const JWT={
    OTP_EXPIRATION_TIME:1*1*2*60*1000 //days*hours*minutes*seconds*miliseconds
}
const SERVICE_NAME="admin";

const FILE_PATH={
    FACULTY:'/static/faculties',
    STUDENT:'/static/students'
};

module.exports = {
    DEFAULT_LIMIT: 10,
    DEFAULT_SKIP: 0,
    LANGUAGE_TYPE,
    LOGGER,
    REQUEST_METHOD,
    STATUS,
    DEVICE_TYPE,
    USER_TYPE,
    OTP_VALIDATION,
    FILE_TYPE,
    CONTENT_TYPE,
    JWT,
    SERVICE_NAME,
    FILE_PATH
};
