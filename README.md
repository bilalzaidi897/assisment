
Step to run this project
1.Config the database connections in config folder
2.Run this command for db migration `node migration-script.js up`.
3.Run this command for start a project npm run start.
OR
3.If you have pm2 then run pm2 command `npm run pm2:dev`
OR
3.If you have nodemon then run command `npm run dev`

4.You can also use redis for auth.`Not in scope of assignment`

5.You can also enable log system `Go to contant folder then go contant file and enable or disable the property of this variable LOGGER`

Note:
1.Patter of url is :domain/api/service name/module name/version/route name
eg.. http://localhost:3081/api/admin/admins/v1/accessToken

Login credentails:
email:demo@yopmail.com
password:Qwerty@123

2.Run mysql and redis both