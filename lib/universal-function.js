const joi = require("joi");
const jwt = require("jsonwebtoken");
const Handlebars = require("handlebars");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");
const path = require('path');
const fs = require('fs');
const config = require("config");

const { LANGUAGE_TYPE, FILE_TYPE } = require("../constant/constant");
const { codes } = require("../codes/status_codes");
const langs = require("../langs");
const { messages } = require("../messages/messages");

const sendResponse = async (req, res, code, message, data) => {
  try {
    const lang = req.headers["content-language"] || LANGUAGE_TYPE.ENGLISH;
    return res.status(200).send({
      statusCode: code || codes.SUCCESS,
      message: langs[lang][message || messages.SUCCESS],
      data: data || {},
    });
  } catch (error) {
    throw error;
  }
};

const sendErrorResponse = async (req, res, code, error) => {
  try {
    const lang = req.headers["content-language"] || LANGUAGE_TYPE.ENGLISH;
    return res.status(208).send({
      statusCode: code || codes.BAD_REQUEST,
      error: error,
      message: langs[lang][error] || error,
    });
  } catch (error) {
    throw new Error(error);
  }
};

const unauthorizedResponse = async (req, res, message) => {
  try {
    const lang = req.headers["content-language"] || LANGUAGE_TYPE.ENGLISH;
    const code = codes.UNAUTHORIZED;
    message = message || messages.UNAUTHORIZED;
    return res.status(code).send({
      statusCode: code,
      message: langs[lang][message],
      data: {},
    });
  } catch (error) {
    throw error;
  }
};

const forBiddenResponse = async (req, res, message) => {
  try {
    const lang = req.headers["content-language"] || LANGUAGE_TYPE.ENGLISH;
    return res.status(208).send({
      statusCode: codes.FORBIDDEN,
      message: langs[lang][message || messages.FORBIDDEN],
      data: {},
    });
  } catch (error) {
    throw error;
  }
};
const sendDownloadResponse = async (req, res, stream, fileType, fileName) => {
  try {
    fileName = fileName || Date.now();
    switch (fileType) {
      case FILE_TYPE.CSV:
        stream.pipe(res);
        res.setHeader("Content-Type", "text/html; charset=utf-8");
        res.setHeader(
          "Content-Disposition",
          'attachment; filename="' + fileName + '.csv"'
        );
        break;
      case FILE_TYPE.EXCEL:
        stream.pipe(res);
        res.setHeader(
          "Content-Type",
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        );
        res.setHeader(
          "Content-Disposition",
          'attachment; filename="' + fileName + '.xlsx"'
        );
    }
    return true;
  } catch (error) {
    throw error;
  }
};

const validateSchema = async (inputs, schema) => {
  try {
    const { error, value } = joi.validate(inputs, schema);
    if (error) throw error.details ? error.details[0].message : "";
    else return false;
  } catch (error) {
    throw error;
  }
};

const jwtSign = async (payload) => {
  try {
    return jwt.sign(payload , config.get("jwtOption.jwtSecretKey"), {
      expiresIn: config.get("jwtOption.expiresIn"),
    });
  } catch (error) {
    throw error;
  }
};

const jwtVerify = async (token) => {
  try {
    return jwt.verify(token, config.get("jwtOption.jwtSecretKey"));
  } catch (error) {
    throw error;
  }
};

const jwtDecode = async (token) => {
  try {
    return jwt.decode(token, { complete: true });
  } catch (error) {
    throw error;
  }
};

const hashPasswordUsingBcrypt = async (plainTextPassword) => {
  return bcrypt.hashSync(plainTextPassword, 10);
};

const comparePasswordUsingBcrypt = async (pass, hash) => {
  return bcrypt.compareSync(pass, hash);
};

const renderMessage = async (templateData, variablesData) => {
  return Handlebars.compile(templateData)(variablesData);
};

const generateNumber = async () => {
  return Math.floor(1000 + Math.random() * 9000).toString();
};

const filterSpacesFromArray = async (arr) => {
  return arr.filter((item) => {
    return item && /\S/.test(item);
  });
};

const makeUniqueArray = async (array) => {
  if (Array.isArray(array)) {
    return [...new Set(array)];
  }
  return array;
};
const removeSpaces = async (value) => {
  if (!value) {
    return value;
  }
  return value.toString().replace(/\s/g, "");
};

const removeNewLineCharacters = async (value) => {
  if (!value) {
    return value;
  }
  return value.toString().replace(/\n|\r/g, "");
};

const dynamicPrecision = async (value, precision) => {
  precision = precision || 2;
  let float_val = parseFloat(value);
  if (isNaN(float_val)) {
    return value;
  }
  return +float_val.toFixed(precision);
};

const addDecimalPrecisionPoint = (apiReference, precision, value) => {
  if (value == null || precision <= 0) {
    return value;
  }
  let values = parseFloat(value);
  return values.toFixed(precision);
};

const convertArrayToObject = async (array) => {
  let arrayLength = array.length;
  let returnObj = {};
  for (let count = 0; count < arrayLength; count++) {
    returnObj[array[count]] = array[count];
  }
  return returnObj;
};

const addingSecurePrefixToURL = async (domainURL) => {
  if (domainURL.indexOf("https://") < 0 && domainURL.indexOf("http://") < 0) {
    domainURL = "https://" + domainURL;
  }
  return domainURL;
};

const addingPrefixToURL = async (domainURL) => {
  if (domainURL.indexOf("https://") < 0 && domainURL.indexOf("http://") < 0) {
    domainURL = "http://" + domainURL;
  }
  return domainURL;
};

const delay = (ms) => new Promise((res) => setTimeout(res, ms));

const generateKeys=async()=> {
  const { privateKey, publicKey } = crypto.generateKeyPairSync('rsa', {
    modulusLength: 4096,
    publicKeyEncoding: {
      type: 'pkcs1',
      format: 'pem',
    },
    privateKeyEncoding: {
      type: 'pkcs1',
      format: 'pem',
      cipher: 'aes-256-cbc',
      passphrase: '',
    },
  })

  fs.writeFileSync('./keys/private.pem', privateKey)
  fs.writeFileSync('./keys/public.pem', publicKey)
};

const encrypt = async (text) => {
  const absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey)
  const publicKey = fs.readFileSync(absolutePath, 'utf8')
  const buffer = Buffer.from(toEncrypt, 'utf8')
  const encrypted = crypto.publicEncrypt(publicKey, buffer)
  return encrypted.toString('base64')
};

const decrypt = async (text) => {
  const absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey)
  const privateKey = fs.readFileSync(absolutePath, 'utf8')
  const buffer = Buffer.from(text, 'base64')
  const decrypted = crypto.privateDecrypt(
    {
      key: privateKey.toString(),
      passphrase: '',
    },
    buffer,
  )
  return decrypted.toString('utf8')
};

const getCallerIP = async (req) => {
  let ip = req.ip || "00.00.000.00";
  return ip;
};

const randomString = async (len, charSet) => {
  charSet =
    charSet ||
    "ABCDEFGHIJKLMNOPQRSTUVW@#$XYZabcdefghijklmnopqrstuvwxyz0123456789";
  var randomString = "";
  for (var i = 0; i < len; i++) {
    var randomPoz = Math.floor(Math.random() * charSet.length);
    randomString += charSet.substring(randomPoz, randomPoz + 1);
  }
  return randomString;
};

module.exports = {
  comparePasswordUsingBcrypt,
  forBiddenResponse,
  hashPasswordUsingBcrypt,
  jwtDecode,
  jwtSign,
  jwtVerify,
  renderMessage,
  sendErrorResponse,
  sendResponse,
  unauthorizedResponse,
  sendDownloadResponse,
  validateSchema,
  generateNumber,
  filterSpacesFromArray,
  makeUniqueArray,
  removeSpaces,
  removeNewLineCharacters,
  dynamicPrecision,
  addDecimalPrecisionPoint,
  convertArrayToObject,
  addingSecurePrefixToURL,
  addingPrefixToURL,
  delay,
  generateKeys,
  encrypt,
  decrypt,
  getCallerIP,
  randomString,
};
