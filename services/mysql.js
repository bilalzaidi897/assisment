const Promise = require("bluebird");
const getConnection = async (transactionConnection) => {
  return new Promise((resolve, reject) => {
    transactionConnection = transactionConnection || connection;
    transactionConnection.getConnection(function (error) {
      if (error) {
        reject(error);
      } else {
        resolve(true);
      }
    });
  });
};
const beginTransaction = async (transactionConnection) => {
  return new Promise((resolve, reject) => {
    transactionConnection = transactionConnection || connection;
    transactionConnection.beginTransaction(function (error) {
      if (error) {
        reject(error);
      } else {
        resolve(true);
      }
    });
  });
};
const queryTransaction = async (sql, params, transactionConnection) => {
  return new Promise((resolve, reject) => {
    transactionConnection = transactionConnection || connection;
    transactionConnection.query(sql, params, function (error, results) {
      if (error) {
        reject(error);
      } else {
        resolve(results);
      }
    });
  });
};
const commit = async (transactionConnection) => {
  return new Promise((resolve, reject) => {
    transactionConnection = transactionConnection || connection;
    transactionConnection.commit(function (error) {
      if (error) {
        reject(error);
      } else {
        resolve(true);
      }
    });
  });
};
const rollback = async (transactionConnection) => {
  return new Promise((resolve, reject) => {
    transactionConnection = transactionConnection || connection;
    transactionConnection.rollback(function () {
      transactionConnection.release();
    });
  });
};
const query = async (sql, params) => {
  return new Promise((resolve, reject) => {
    connection.query(sql, params, async function (error, results) {
      if (error) {
        reject(error);
      } else {
        resolve(results);
      }
    });
  });
};
const executeQuery = async (sql, params) => {
  return new Promise((resolve, reject) => {
    slaveConnection.query(sql, params, function (error, results) {
      if (error) {
        reject(error);
      } else {
        resolve(results);
      }
    });
  });
};
const processMysqlDataStream = async (apiReference, options) => {
  return new Promise((resolve, reject) => {
    let iter = 0;
    let queryData = [];
    connection.getConnection(function (error, poolCon) {
      if (error) {
        reject();
      }
      poolCon._isConnectionReleased = false;
      const query = poolCon.query(options.sqlQuery);
      query.on("error", function (err) {
        // Handle error, an 'end' event will be emitted after this as well
        poolCon.release();
        poolCon._isConnectionReleased = true;
      });

      query.on("result", function (row) {
        Promise.coroutine(function* () {
          // Pausing the connnection is useful if your processing involves I/O
          queryData.push(row);
          iter += 1;
          if (iter === options.limit) {
            poolCon.pause();
            // process the data
            yield options.processQueryData(
              apiReference,
              queryData,
              options.metaData
            );
            queryData = [];
            iter = 0; // processed the data set the iter to 0
            poolCon.resume();
          }
        })();
      });

      query.on("end", function () {
        Promise.coroutine(function* () {
          // Pausing the connnection is useful if your processing involves I/O
          if (iter !== 0) {
            // process the data
            yield options.processQueryData(
              apiReference,
              queryData,
              options.metaData
            );
            queryData = [];
          }
        })();
        if (poolCon._isConnectionReleased === false) {
          poolCon.release();
        }
        resolve();
      });
    });
  });
};
const stremaQuery = async (sql, params) => {
  try {
    return connection.query(sql, params).stream();
  } catch (error) {
    connection.release();
    throw error;
  }
};
module.exports = {
  getConnection,
  beginTransaction,
  queryTransaction,
  commit,
  rollback,
  query,
  executeQuery,
  processMysqlDataStream,
  stremaQuery,
};
