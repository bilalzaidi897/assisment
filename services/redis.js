"use strict";
const setData = async (key, value) => {
  try {
    return await redisConnect.setAsync(key, value);
  } catch (error) {
    throw new Error(error);
  }
};
const setTime = async (key, ms = 1000) => {
  try {
    return await redisConnect.expire(key, ms);
  } catch (error) {
    throw new Error(error);
  }
};
const getData = async (key) => {
  try {
    return await redisConnect.getAsync(key);
  } catch (error) {
    throw new Error(error);
  }
};
const deleteData = async (key) => {
  try {
    return await redisConnect.delAsync(key);
  } catch (error) {
    throw new Error(error);
  }
};
const hSetData = async (hkey, key, value) => {
  try {
    return await redisConnect.hsetAsync(hkey, key, value);
  } catch (error) {
    throw new Error(error);
  }
};
const hGetData = async (hkey, key) => {
  try {
    return await redisConnect.hgetAsync(hkey, key);
  } catch (error) {
    throw new Error(error);
  }
};
const hGetAllData = async (hkey) => {
  try {
    return await redisConnect.hgetallAsync(hkey);
  } catch (error) {
    throw new Error(error);
  }
};
const hDeleteData = async (hkey, key) => {
  try {
    return await redisConnect.hdelAsync(hkey, key);
  } catch (error) {
    throw new Error(error);
  }
};
const hExistsData = async (hkey, key) => {
  try {
    return await redisConnect.hexistsAsync(hkey, key);
  } catch (error) {
    throw new Error(error);
  }
};
const hmSetData = async (hkey, obj) => {
  try {
    return await redisConnect.hmsetAsync(hkey, obj);
  } catch (error) {
    throw new Error(error);
  }
};
const hmGetData = async (hmkey, arr) => {
  try {
    return await redisConnect.hmgetAsync(hmkey, arr);
  } catch (error) {
    throw new Error(error);
  }
};
const hmDeleteData = async (hmkey) => {
  try {
    return await redisConnect.hmdelAsync(hmkey);
  } catch (error) {
    throw new Error(error);
  }
};
const hmExistsData = async (hkey, key) => {
  try {
    return await redisConnect.hmexistsAsync(hkey, key);
  } catch (error) {
    throw new Error(error);
  }
};
module.exports = {
  getData,
  setData,
  setTime,
  deleteData,
  hSetData,
  hGetData,
  hDeleteData,
  hExistsData,
  hmSetData,
  hmGetData,
  hmDeleteData,
  hmExistsData,
  hGetAllData,
};
