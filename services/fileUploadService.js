const multer = require('multer');

const facultyUpload = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/faculties')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + `.${file.originalname.split('.').pop()}`)
    }
});
const studentUpload = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/students')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + `.${file.originalname.split('.').pop()}`)
    }
});

const faculty = multer({ storage: facultyUpload });
const student = multer({ storage: studentUpload });
module.exports = {
    faculty: faculty,
    student: student
};
